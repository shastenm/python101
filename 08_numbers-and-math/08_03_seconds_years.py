# Move the code you previously wrote to calculate
# how many seconds are in a year into this file.
# Then execute it as a script and print the output to your console.

minutes = 60
hour = 60
day = 24
year = 365

seconds_year = minutes * hour * day * year
print(seconds_year)

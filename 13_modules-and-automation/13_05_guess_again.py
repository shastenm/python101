# Re-create the guess-my-number game from scratch. Don't peek!
# This time, give your players only a certain amount of tries 
# before they lose.

import random
import sys

random_num = random.randrange(0, 101)


guess = int(input('Guess the number from 1-100: '))
count = 5

while guess != random_num:
    count -= 1
    if count > 0:
        if guess > random_num:
            print('Lower')
            guess = int(input('Guess the number from 1-100: '))
        elif guess < random_num:
            print('Higher')
            guess = int(input('Guess the number from 1-100: '))
    elif count == 0:
        print('You lose')
        sys.exit()
    else:
        print(f'{guess} was the number')
        break



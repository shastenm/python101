# Use the built-in `platform` module to print out the following info:
import platform
placeholder = "remove me :)"
 
print(f"{'Platform:':>10} {placeholder}",platform.platform())  # platform.platform()
print(f"{'Compiler:':>10} {placeholder}", platform.python_compiler())  # platform.python_compiler()
print(f"{'Python:':>10} {placeholder}", platform.python_version())  # platform.python_version()
print(f"{'System:':>10} {placeholder}", platform.system())  # platform.system()
print(f"{'Release:':>10} {placeholder}", platform.release())  # platform.release()
print(f"{'System:':>10} {placeholder}", platform.processor())  # platform.processor()

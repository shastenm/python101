sentence = (''' Write a script that takes a string input from a user
 and prints a total count of how often each individual vowel appeared.''').lower()


print(f'A:', sentence.count('a'))
print(f'E:', sentence.count('e'))
print(f'I:', sentence.count('i'))
print(f'O:', sentence.count('o'))
print(f'U:', sentence.count('u'))




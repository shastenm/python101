# Take in the following three values from the user:
# 1. investment amount
# 2. interest rate in percentage
# 3. number of years to invest
#
# Calculate the future values and print them to the console.

def user_invests(a):
    #return 4% investment
    return a * 0.04
investment = user_invests(50000)


def years_invested(a):
    return a // investment
user_goal = years_invested(50000)
    
print(f'It will take {user_goal} years to make $50,000 on an initial $50,000 investment')

# Write a program that takes a number between 1 and 1,000,000,000
# from the user and determines whether it is divisible by 3 using an `if` statement.
# Print the result.
import random

random_number = random.randrange(1, 1000000)

if random_number % 3 == 0:
    print(f'{random_number} is divisible by 3.')
else:
    print(f'{random_number} is not divisible by 3.')

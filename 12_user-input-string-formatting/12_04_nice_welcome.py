# Ask the user to input their name. Then print a nice welcome message
# that welcomes them personally to your script.
# If a user enters more than one name, e.g. "firstname lastname",
# then use only their first name to overstep some personal boundaries
# in your welcome message.

name = input('Enter Your Name: ').capitalize().split()

print(f'Welcome {name[0]}, I hope you have a pleasant experience playing this game.')
